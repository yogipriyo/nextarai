//root file for the app
import React, { Component } from "react";
import Router from './src/RouterComponent';

console.disableYellowBox = true;

class App extends Component {
  render (){
    return(
      <Router />
    );
  }
}

export default App;