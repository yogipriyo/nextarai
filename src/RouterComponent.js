import React from 'react'
import { Scene, Router, ActionConst, Actions } from 'react-native-router-flux'
import Homepage from './pages/Homepage'
import ClientPage from './pages/ClientPage'
import { View, Text, StyleSheet,Image,TouchableOpacity } from 'react-native'
import { BASE_COLOR, APP_NAME } from './Config';
import Icon from 'react-native-vector-icons/FontAwesome';
import NavBar from './components/NavBar';

const style = StyleSheet.create({
  appBar: {
    backgroundColor: "white",
    paddingRight: 10
  },
  titleBar: {
    color : "black", 
    fontSize: 20, 
    fontWeight: 'bold', 
    paddingLeft: 20,
  }
})

const FavButton = () => {
  return (
    <View style={{paddingRight: 15}}>
      <Icon name={'bug'} size={30} color="black"/>
    </View>
  )
}

const RouterComponent = () => {
  var duration = 50;
  return (
    <Router 
      navigationBarStyle={style.appBar} 
      titleStyle={style.titleBar} 
      leftButtonIconStyle = {{ tintColor:'white'}} 
      renderRightButton={() => { return <FavButton /> }}
    >
      <Scene key="root"> 
        <Scene key="homepage" title={APP_NAME} component={Homepage} hideNavBar={false} duration={duration} initial/>
        <Scene key="clientPage" component={ClientPage} hideNavBar={false} duration={duration} backTitle=" "/>
      </Scene>
    </Router>
  );
};

export default RouterComponent;