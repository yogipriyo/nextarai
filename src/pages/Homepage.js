import React, { Component } from 'react';
import { ScrollView, Text, View, Image, StyleSheet, List, TouchableOpacity, SafeAreaView, StatusBar, FlatList } from 'react-native';
import styles, { colors } from '../styles/index.styles';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.styles';
import { Actions } from 'react-native-router-flux';
import Carousel from 'react-native-snap-carousel';
import { ENTRIES1 } from '../pages/entries';
import SliderEntry from '../pages/SliderEntry';
import {SwipeableFlatList} from 'react-native-swipeable-flat-list';

const SLIDER_1_FIRST_ITEM = 1;
const TODOS = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        content: 'Allison from Courts has not responded in 3 days. Follow up?',
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png',
        leftLabel: 'Dismiss', 
        rightLabel: 'Options' 
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        content: 'Have you sent the signed NDA to Singapore Airlines?',
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png',
        leftLabel: 'Dismiss', 
        rightLabel: 'Options' 
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        content: 'Prepare for your meeting tomorrow with Carl from Singapore Airlines',
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png',
        leftLabel: 'Dismiss', 
        rightLabel: 'Options' 
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d73',
        content: 'Finish debrief for yesterday meeting with Sam from Nestle',
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png',
        leftLabel: 'Dismiss', 
        rightLabel: 'Options' 
    },
];

const DEALSLEADS = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
        progress: 0.5,
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png'
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
        progress: 0.5,
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
        progress: 0.5,
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d73',
        title: 'Fourth Item',
        progress: 0.5,
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png'
    },
];

class Homepage extends Component{

    constructor(props) {
        super(props);

        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM
        };
    }

    _renderItem ({item, index}) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }

    _renderItemWithParallax ({item, index}, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
            />
        );
    }

    mainExample (number, title) {
        const { slider1ActiveSlide } = this.state;

        return (
            <View style={styles.exampleContainer}>
                <Text style={styles.title}>How you're doing</Text>
                <Carousel
                    ref={c => this._slider1Ref = c}
                    data={ENTRIES1}
                    renderItem={this._renderItemWithParallax}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    hasParallaxImages={true}
                    firstItem={SLIDER_1_FIRST_ITEM}
                    inactiveSlideScale={0.94}
                    inactiveSlideOpacity={0.7}
                    // inactiveSlideShift={20}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    loop={true}
                    loopClonesPerSide={2}
                    autoplay={true}
                    autoplayDelay={500}
                    autoplayInterval={3000}
                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
            </View>
        );
    }

    render(){
        const example1 = this.mainExample(1, 'Default layout | Loop | Autoplay');
        return(
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <ScrollView
                      style={styles.scrollview}
                      scrollEventThrottle={200}
                      directionalLockEnabled={true}
                    >
                        <View>
                            { example1 }
                        </View>
                        <View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={styles.title}>Things to do</Text>
                                <View style={{
                                    height: 20,
                                    width: 20,
                                    borderRadius: 20/2,
                                    backgroundColor: 'white',
                                    alignContent: "center",
                                    justifyContent: "center",
                                }}
                                >
                                    <Text style={styles.centeredText}>4</Text>
                                </View>
                            </View>
                            <SwipeableFlatList
                                style={{top: 15}}
                                backgroundColor='#000'
                                data={TODOS}
                                showsVerticalScrollIndicator={false}
                                renderItem={({item}) =>
                                <View style={{flex: 1, backgroundColor: colors.background1}}>
                                    <View style={internalStyles.item}>
                                        <Image
                                            style={{width: 35, height: 35, borderRadius: 10, marginRight: 8}}
                                            source={{uri: item.image}}
                                        />
                                        <TouchableOpacity style={{flex: 1,justifyContent: 'center'}} onPress={() => Actions.clientPage({ title: 'Singapore Airlines' })}>
                                            <Text style={styles.normalText}>{item.content}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                }
                                renderLeft={({ item }) => (
                                    <View style={{ width: 100, height: 60, justifyContent: 'center', alignContent: 'center', backgroundColor: 'red'}}>
                                        <Text style={{ color: 'white', fontWeight: 'bold' , textAlign: 'center'}}>{item.leftLabel}</Text>
                                    </View>
                                )}
                                renderRight={({ item }) => (
                                    <View style={{ width: 100, height: 60, justifyContent: 'center', alignContent: 'center', backgroundColor: 'green'}}>
                                        <Text style={{ color: 'white', fontWeight: 'bold' , textAlign: 'center'}}>{item.rightLabel}</Text>
                                    </View>
                                )}
                            keyExtractor={item => item.id} />
                        </View>
                        <View style={{top: 10, marginTop: 20, marginBottom: 20}}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={styles.title}>Deals and Leads</Text>
                                <View style={{
                                    height: 20,
                                    width: 20,
                                    borderRadius: 20/2,
                                    backgroundColor: 'white',
                                    alignContent: "center",
                                    justifyContent: "center",}}
                                >
                                    <Text style={styles.centeredText}>4</Text>
                                </View>
                            </View>
                            <FlatList
                                style={{top: 15}}
                                data={DEALSLEADS}
                                showsVerticalScrollIndicator={false}
                                renderItem={({item}) =>
                                <View style={[internalStyles.item, {height: 80}]}>
                                    <Image
                                        style={{width: 50, height: 50, borderRadius: 20, marginRight: 8}}
                                        source={{uri: item.image}}
                                    />
                                    <View style={{justifyContent: 'center'}}>
                                        <Text style={styles.normalText}>{item.title}</Text>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-end', backgroundColor: 'transparent', height: 60, marginLeft: 5}}>
                                        <View style={{ backgroundColor: 'white', width: 5, height: 60, borderRadius: 2.5, borderColor: '#55AAFF', borderWidth: 1, top: 0, position: 'absolute'}} />
                                        <View style={{ backgroundColor: 'white', width: 5, height: 30, borderRadius: 2.5, borderColor: '#55AAFF', borderWidth: 1, bottom: 0, backgroundColor: '#55AAFF', position: 'absolute'}} />
                                    </View>
                                </View>
                                }
                                numColumns={2}
                            keyExtractor={item => item.id} />
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }

}

const internalStyles = StyleSheet.create({
    item: {
        flex:1,
        flexDirection: 'row',
        backgroundColor: 'white',
        height: 60,
        padding: 8,
        marginVertical: 2,
        marginHorizontal: 4,
        alignContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 5
    },
    title: {
      fontSize: 32,
    },
});

export default Homepage;