import React, { Component } from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, ScrollView, FlatList } from 'react-native';
import styles, { colors } from '../styles/index.styles';
import { Actions } from 'react-native-router-flux';

const TODOS = [
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        content: 'Have you sent the signed NDA to Singapore Airlines?',
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        content: 'Prepare for your meeting tomorrow with Carl from Singapore Airlines',
        image: 'https://facebook.github.io/react-native/img/tiny_logo.png'
    }
];

class ClientPage extends Component{

  render(){
    return(
        <SafeAreaView style={styles.safeArea}>
            <View style={styles.container}>
                <ScrollView
                    style={styles.scrollview}
                    scrollEventThrottle={200}                      
                    directionalLockEnabled={true}>
                        <View style={{flexDirection: 'row', top: 15}}>
                            <Text style={styles.title}>Things to do</Text>
                            <View style={{
                                height: 20,
                                width: 20,
                                borderRadius: 20/2,
                                backgroundColor: 'white',
                                alignContent: "center",
                                justifyContent: "center",
                            }}
                            >
                                <Text style={styles.centeredText}>4</Text>
                            </View>
                            </View>
                            <FlatList
                                style={{top: 25}}
                                data={TODOS}
                                showsVerticalScrollIndicator={false}
                                renderItem={({item}) =>
                                <View style={internalStyles.item}>
                                    <Image
                                        style={{width: 35, height: 35, borderRadius: 10, marginRight: 8}}
                                        source={{uri: item.image}}
                                    />
                                    <View style={{flex: 1,justifyContent: 'center'}}>
                                        <Text style={styles.normalText}>{item.content}</Text>
                                    </View>
                                </View>
                                }
                            keyExtractor={item => item.id} />
                </ScrollView>
            </View>
        </SafeAreaView>
    );
  }

}

const internalStyles = StyleSheet.create({
    item: {
        flex:1,
        flexDirection: 'row',
        backgroundColor: 'white',
        height: 60,
        padding: 8,
        marginVertical: 2,
        marginHorizontal: 4,
        alignContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 5
    },
    title: {
      fontSize: 32,
    },
});

export default ClientPage;