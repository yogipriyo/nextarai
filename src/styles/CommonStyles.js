import React, {
    StyleSheet
  } from 'react-native';
  
  module.exports = StyleSheet.create({
    rootContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white'
    },
    rootContainer2: {
      flex: 1,
      backgroundColor: '#01b2fe'
    },
    formContainer: {
      backgroundColor: 'white',
      borderColor: 'black',
      borderWidth: StyleSheet.hairlineWidth,
      // borderRadius: 10
    },
    topSection: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: '#01b2fe',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
    midSection: {
      flex: 6,
      marginTop: 20,
      marginBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
      flexDirection: 'column',
      justifyContent: 'space-between',
      // backgroundColor: 'red'
    },
    lowerSection: {
      flex: 2,
      paddingLeft: 20,
      paddingRight: 20,
      borderColor: '#01b2fe',
      borderTopWidth: StyleSheet.hairlineWidth,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      // backgroundColor: 'yellow'
    },
    topSectionModal: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: '#01b2fe',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
    midSectionModal: {
      flex: 6,
      marginTop: 20,
      marginBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
      flexDirection: 'column',
      justifyContent: 'space-between',
      // backgroundColor: 'red'
    },
    lowerSectionModal: {
      flex: 1,
      // paddingLeft: 20,
      // paddingRight: 20,
      // borderColor: '#01b2fe',
      // borderTopWidth: StyleSheet.hairlineWidth,
      flexDirection: 'row',
      // justifyContent: 'space-around',
      alignItems: 'center',
      // backgroundColor: 'yellow'
    },
    textField: {
      marginTop: 10,
      borderColor: '#01b2fe',
      borderWidth: 1,
    },
    roundedButton: {
      height: 40,
      borderColor: '#01b2fe',
      borderRadius: 5,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'black'
    },
    textButton: {
      color: 'white',
      textAlign: 'center'
    },
    modalContainer: {
      backgroundColor: 'white',
      borderColor: 'black',
      borderWidth: StyleSheet.hairlineWidth,
      // borderRadius: 10,
      margin: 70,
      marginTop: 150,
      // alignItems: 'flex-start'
    }
  });